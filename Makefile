OBJS	= main.o
SOURCE	= main.cpp
HEADER	= EnvVars.h
OUT	= main
CC	 = g++
FLAGS	 = -g -c -Wall -std=c++14
LFLAGS	 =

all: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT) $(LFLAGS)

main.o: main.cpp
	$(CC) $(FLAGS) main.cpp

clean:
	rm -f $(OBJS) $(OUT)

run: $(OUT)
	./$(OUT)