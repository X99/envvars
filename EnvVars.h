/**
 * @file EnvVars.h
 * @author X99
 * @brief Manages environment variables in order to use them into a piece of code.
 * @version 0.1
 * @date 2021-07-19
 *
 * @copyright Copyright (c) 2021 X99
 */

#pragma once

#include <map>
#include <vector>
#include <sstream>
#include <string>
#include <initializer_list>
#include <exception>

#include <iostream>

struct env_var {
    std::string var_name = "";
    std::string value;
    bool is_mandatory = false;

    std::string to_string() const {
        std::stringstream ss;
        ss << var_name << ": " << value;
        if (is_mandatory == true)
            ss << " (mandatory)";
        ss << "\n";
        return ss.str();
    }
};

class MandatoryValueException : public std::exception {
    std::string message;

 public:
    explicit MandatoryValueException(std::string value_name) {
        message = std::string("ERROR: mandatory value \"") + value_name + std::string("\" could not be found.");
    }

    virtual const char * what() const throw() {
        return message.c_str();
    }
};

class EnvVars {
 private:
    std::map<std::string, env_var> env_vars{};

 public:
    EnvVars() {}

    explicit EnvVars(const std::initializer_list<env_var>& list) {
        for (auto& e : list) {
            if (add_var(e) == false && e.is_mandatory == true)
                throw MandatoryValueException(e.var_name);
        }
    }
    explicit EnvVars(const std::vector<env_var>& vect) {
        for (auto& e : vect) {
            if (add_var(e) == false && e.is_mandatory == true)
                throw MandatoryValueException(e.var_name);
        }
    }

    explicit EnvVars(const std::vector<std::string>& vect) {
        for (const std::string &e : vect) {
            env_var var {.var_name = e};
            add_var(var);
        }
    }

    bool add_var(env_var v) {
        char *p = std::getenv(v.var_name.c_str());

        v.value = (p != nullptr? std::string(p):std::string());

        env_vars[v.var_name] = v;

        return (p != nullptr);
    }

    const env_var operator[](std::string var) const {
        return env_vars.at(var);
    }

    std::string to_string(std::string var) {
        env_var v = env_vars[var];

        return v.to_string();
    }
};
