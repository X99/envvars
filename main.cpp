#include <iostream>
#include <vector>

#include "EnvVars.h"

int main() {
    /*
        First batch: simple env vars.
    */
    std::cout << "First batch with initializer_list constructor" << std::endl;

    EnvVars test{
        {.var_name = "HOME", .is_mandatory = true},
        {.var_name = "unknown", .is_mandatory = false}
    };

    std::cout << test.to_string("HOME");
    std::cout << test.to_string("unknown");


    /*
        Second batch: constructs a list from a std::vector of env_var struc.
    */
    std::cout << "\nSecond batch with std::vector of env_var constructor" << std::endl;

    std::vector<env_var> test2_data{
        {.var_name = "HOME"},
        {.var_name = "PATH"},
        {.var_name = "CUSTOM_INT"}
    };

    EnvVars test2{test2_data};

    for (auto e : test2_data)
        std::cout << test2.to_string(e.var_name);



    /*
        Third batch: constructs a list from a std::vector of std::string
    */
    std::cout << "\nThird batch with std::vector of std::string constructor" << std::endl;

    std::vector<std::string> test3_data{
        "HOME",
        "PATH",
        "CUSTOM_INT"
    };

    EnvVars test3{test3_data};
    for (std::string s : test3_data)
        std::cout << test3.to_string(s);



    /*
        Fourth batch: exception throwing because of a missing mandatory env var.
    */
    std::cout << "\nFourth batch with mandatory string exception" << std::endl;

    try {
         EnvVars test{
            {.var_name = "HOME", .is_mandatory = true},
            {.var_name = "unknown", .is_mandatory = true}
        };
    } catch(const MandatoryValueException& e) {
        std::cerr << e.what() << '\n';
    }



    return 0;
}
